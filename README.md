# Synopsis
Generate ssl certificate that last 10 years automatically.

# Code Example
	
# Motivation
To simplify the ordinary routines on generating ssl certificate.

# Installation
Download the source code and run the installation script as follows.
```bash

```

# API Reference

# Tests
Run the application on terminal.
```bash
./sslKeyGen
```

# Contributor
Jason Chen
